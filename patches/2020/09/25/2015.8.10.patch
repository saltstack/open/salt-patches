From 495b4b471996ce2fc367ee39f1ce18f1be645e5e Mon Sep 17 00:00:00 2001
From: "Daniel A. Wozniak" <dwozniak@saltstack.com>
Date: Wed, 16 Sep 2020 00:27:54 +0000
Subject: [PATCH] Fix CVE-2020-25592

---
 salt/config.py          |   5 +
 salt/netapi/__init__.py | 209 +++++++++++++++++++++++++++++++++++++++-
 2 files changed, 213 insertions(+), 1 deletion(-)

diff --git a/salt/config.py b/salt/config.py
index 5321e77553..116ff514e9 100644
--- a/salt/config.py
+++ b/salt/config.py
@@ -775,6 +775,10 @@ VALID_OPTS = {
 
     # Delay in seconds before executing bootstrap (salt cloud)
     'bootstrap_delay': int,
+
+    # Allow raw_shell option when using the ssh
+    # client via the Salt API
+    'netapi_allow_raw_shell': bool,
 }
 
 # default configurations
@@ -1227,6 +1231,7 @@ DEFAULT_MASTER_OPTS = {
     'dummy_pub': False,
     'http_request_timeout': 1 * 60 * 60.0,  # 1 hour
     'http_max_body': 100 * 1024 * 1024 * 1024,  # 100GB
+    'netapi_allow_raw_shell': False,
 }
 
 
diff --git a/salt/netapi/__init__.py b/salt/netapi/__init__.py
index 69f6f95ae2..e1c9f6470c 100644
--- a/salt/netapi/__init__.py
+++ b/salt/netapi/__init__.py
@@ -4,19 +4,32 @@ Make api awesomeness
 '''
 from __future__ import absolute_import
 # Import Python libs
+import copy
 import inspect
+import logging
 import os
 
 # Import Salt libs
 import salt.log  # pylint: disable=W0611
+import salt.auth
 import salt.client
 import salt.config
+import salt.daemons.masterapi
 import salt.runner
 import salt.syspaths
 import salt.wheel
 import salt.utils
+import salt.roster
 import salt.client.ssh.client
 import salt.exceptions
+import salt.utils.args
+import salt.utils.minions
+import salt.wheel
+from salt.defaults import DEFAULT_TARGET_DELIM
+import six
+
+
+log = logging.getLogger(__name__)
 
 
 class NetapiClient(object):
@@ -30,6 +43,15 @@ class NetapiClient(object):
     '''
     def __init__(self, opts):
         self.opts = opts
+        apiopts = copy.deepcopy(self.opts)
+        apiopts["enable_ssh_minions"] = True
+        apiopts["cachedir"] = os.path.join(opts["cachedir"], "saltapi")
+        if not os.path.exists(apiopts["cachedir"]):
+            os.makedirs(apiopts["cachedir"])
+        self.resolver = salt.auth.Resolver(apiopts)
+        self.loadauth = salt.auth.LoadAuth(apiopts)
+        self.key = salt.daemons.masterapi.access_keys(apiopts)
+        self.ckminions = salt.utils.minions.CkMinions(apiopts)
 
     def _is_master_running(self):
         '''
@@ -46,6 +68,183 @@ class NetapiClient(object):
             self.opts['sock_dir'],
             ipc_file))
 
+    def _authorize_token(self, low):
+        # A token was passed, check it
+        try:
+            token = self.resolver.get_token(low['token'].encode())
+        except Exception as exc:
+            log.error(
+                'Exception occurred when generating auth token: {0}'.format(
+                    exc
+                )
+            )
+            error = 'Eauth fail'
+            raise salt.exceptions.EauthAuthenticationError(error)
+
+        # Bail if the token is empty or if the eauth type specified is not allowed
+        if not token or token['eauth'] not in self.opts['external_auth']:
+            log.warning('Authentication failure of type "token" occurred.')
+            error = 'Eauth fail'
+            raise salt.exceptions.EauthAuthenticationError(error)
+
+        # Fetch eauth config and collect users and groups configured for access
+        eauth_config = self.opts['external_auth'][token['eauth']]
+        eauth_users = []
+        eauth_groups = []
+        for entry in eauth_config:
+            if entry.endswith('%'):
+                eauth_groups.append(entry.rstrip('%'))
+            else:
+                eauth_users.append(entry)
+
+        # If there are groups in the token, check if any of them are listed in the eauth config
+        group_auth_match = False
+        try:
+            if token.get('groups'):
+                for group in token['groups']:
+                    if group in eauth_groups:
+                        group_auth_match = True
+                        break
+        except KeyError:
+            pass
+        if '^model' not in eauth_users and '*' not in eauth_users and token['name'] not in eauth_users \
+            and not group_auth_match:
+            log.warning('Authentication failure of type "token" occurred.')
+            return ''
+
+        # Compile list of authorized actions for the user
+        auth_list = token.get('auth_list')
+        if auth_list is None:
+            auth_list = []
+            # Add permissions for '*' or user-specific to the auth list
+            for user_key in ('*', token['name']):
+                auth_list.extend(eauth_config.get(user_key, []))
+            # Add any add'l permissions allowed by group membership
+            if group_auth_match:
+                auth_list = self.ckminions.fill_auth_list_from_groups(eauth_config, token['groups'], auth_list)
+            if token['eauth'] == 'ldap':
+                auth_list = self.ckminions.fill_auth_list_from_ou(auth_list, self.opts)
+        log.trace("Compiled auth_list: {0}".format(auth_list))
+
+        fun = low['fun']
+        if isinstance(low['fun'], six.text_type):
+            if not isinstance(low['fun'], str):
+                fun = low['fun'].encode()
+        good = self.ckminions.auth_check(
+            auth_list,
+            fun,
+            low.get('arg', []),
+            low['tgt'],
+            low.get('tgt_type', 'glob'))
+        roster = salt.roster.Roster(self.opts, self.opts.get('roster', 'flat'))
+        try:
+            minions = [x for x in roster.targets(low['tgt'], low.get('tgt_type', 'glob'))]
+        except salt.exceptions.SaltSystemExit as exc:
+            minions = []
+        if not good:
+            raise salt.exceptions.EauthAuthenticationError(
+                "Authorization error occurred."
+            )
+        if not minions:
+            raise salt.exceptions.EauthAuthenticationError(
+                "Authorization error occurred."
+            )
+        low['user'] = token['name']
+        log.debug('Minion tokenized user = "{0}"'.format(low['user']))
+
+    def _authorize_eauth(self, low):
+        name = self.loadauth.load_name(low)  # The username we are attempting to auth with
+        groups = self.loadauth.get_groups(low)  # The groups this user belongs to
+        if groups is None or groups is False:
+            groups = []
+        group_perm_keys = [item for item in self.opts['external_auth'][low['eauth']] if item.endswith('%')]  # The configured auth groups
+
+        # First we need to know if the user is allowed to proceed via any of their group memberships.
+        group_auth_match = False
+        for group_config in group_perm_keys:
+            group_config = group_config.rstrip('%')
+            for group in groups:
+                if group == group_config:
+                    group_auth_match = True
+        # If a group_auth_match is set it means only that we have a
+        # user which matches at least one or more of the groups defined
+        # in the configuration file.
+
+        external_auth_in_db = False
+        for entry in self.opts['external_auth'][low['eauth']]:
+            if entry.startswith('^'):
+                external_auth_in_db = True
+                break
+
+        # If neither a catchall, a named membership or a group
+        # membership is found, there is no need to continue. Simply
+        # deny the user access.
+        if not ((name in self.opts['external_auth'][low['eauth']]) |
+                ('*' in self.opts['external_auth'][low['eauth']]) |
+                group_auth_match | external_auth_in_db):
+
+            # Auth successful, but no matching user found in config
+            msg = 'Authentication failure of type "eauth" occurred.'
+            log.warning(
+                msg
+            )
+            raise salt.exceptions.EauthAuthenticationError(msg)
+        auth_ret = self.loadauth.time_auth(low)
+        if not auth_ret:
+            error = 'Eauth fail'
+            raise salt.exceptions.EauthAuthenticationError(error)
+        elif isinstance(auth_ret, list):
+            auth_list = auth_ret
+        else:
+            auth_list = []
+            if '*' in self.opts['external_auth'][low['eauth']]:
+                auth_list.extend(self.opts['external_auth'][low['eauth']]['*'])
+            if name in self.opts['external_auth'][low['eauth']]:
+                auth_list = self.opts['external_auth'][low['eauth']][name]
+            if group_auth_match:
+                auth_list = self.ckminions.fill_auth_list_from_groups(
+                        self.opts['external_auth'][low['eauth']],
+                        groups,
+                        auth_list)
+            if low['eauth'] == 'ldap':
+                auth_list = self.ckminions.fill_auth_list_from_ou(auth_list, self.opts)
+        delimiter = low.get("kwargs", {}).get("delimiter", DEFAULT_TARGET_DELIM)
+        roster = salt.roster.Roster(self.opts, self.opts.get('roster', 'flat'))
+        try:
+            minions = [x for x in roster.targets(low['tgt'], low.get('tgt_type', 'glob'))]
+        except salt.exceptions.SaltSystemExit as exc:
+            minions = []
+        fun = low['fun']
+        if isinstance(low['fun'], six.text_type):
+            if not isinstance(low['fun'], str):
+                fun = low['fun'].encode()
+        authorized = self.ckminions.auth_check(
+            auth_list,
+            fun,
+            low.get("arg", []),
+            low["tgt"],
+            low.get("tgt_type", "glob"),
+            publish_validate=True,
+        )
+        if not authorized:
+            raise salt.exceptions.EauthAuthenticationError(
+                "Authorization error occurred."
+            )
+        if not minions:
+            raise salt.exceptions.EauthAuthenticationError(
+                "Authorization error occurred."
+            )
+
+    def _authorize_ssh(self, low):
+        if 'token' in low:
+            return self._authorize_token(low)
+        elif 'eauth' in low:
+            return self._authorize_eauth(low)
+        else:
+            raise salt.exceptions.EauthAuthenticationError(
+                "Authorization error occurred."
+            )
+
     def run(self, low):
         '''
         Execute the specified function in the specified client by passing the
@@ -61,10 +260,18 @@ class NetapiClient(object):
         if 'client' not in low:
             raise salt.exceptions.SaltException('No client specified')
 
-        if not ('token' in low or 'eauth' in low) and low['client'] != 'ssh':
+        if not ('token' in low or 'eauth' in low):
             raise salt.exceptions.EauthAuthenticationError(
                     'No authentication credentials given')
 
+        if low.get('raw_shell') and \
+                not self.opts.get('netapi_allow_raw_shell'):
+            raise salt.exceptions.EauthAuthenticationError(
+                    'Raw shell option not allowed.')
+
+        if low['client'] == 'ssh':
+            self._authorize_ssh(low)
+
         l_fun = getattr(self, low['client'])
         f_call = salt.utils.format_call(l_fun, low)
         return l_fun(*f_call.get('args', ()), **f_call.get('kwargs', {}))
-- 
2.26.2

