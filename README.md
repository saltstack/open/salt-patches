# !!Project is Archived!!

> Salt Project has archived the this repository, and will no longer be updating it. This repository was used for packaging v3005.x and earlier releases of salt ("classic" packaging), which are now EOL.
>
> Salt Project now uses [relenv](https://github.com/saltstack/relenv) as part of creating more universal packages, that include a build of Python with other tools, and builds/releases packages via GitHub Action workflows:
> - https://github.com/saltstack/salt/blob/master/.github/workflows/nightly.yml
> - https://github.com/saltstack/salt/blob/master/.github/workflows/staging.yml
> - https://github.com/saltstack/salt/blob/master/.github/workflows/release.yml

# Salt Patches

General and CVE related patches for Salt.


## List of patches

[OpenSSL 1.0 Fix](./patches/2016/11/21/0001-Fix-openssl-1.0.patch)

**May 13, 2020 CVE Release**

[CVE-2020-11651 and CVE-2020-11652](./patches/2020/04/14/README.md)

**November 3, 2020 CVE Release**

[CVE-2020-16846 and CVE-2020-17490](./patches/2020/09/02/README.md)

[CVE-2020-25592](./patches/2020/09/25/README.md)

**February 25, 2021 CVE Release**

[CVE-2020-28243 CVE-2020-28972 CVE-2020-35662 CVE-2021-3148 CVE-2021-3144
 CVE-2021-25281 CVE-2021-25282 CVE-2021-25283 CVE-2021-25284
 CVE-2021-3197](./patches/2021/01/28/README.md)

[Addtional patches for CVE-2020-28243 CVE-2020-28972 CVE-2020-35662
CVE-2021-3148 CVE-2021-3144 CVE-2021-25281 CVE-2021-25282 CVE-2021-25283
CVE-2021-25284 CVE-2021-3197](./patches/2021/02/05/README.md)
